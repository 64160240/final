package com.chaiwat;

public class Food {
    private int number;
    private String name;
    private int price;

    public Food(int number,String name,int price){
        this.number = number;
        this.name = name;
        this.price = price;
    }
    public int getPrice(){
        return price;
    }
    public int getNumber(){
        return number;
    }
    public String getName(){
        return name;
    }
    public String toString(){
        return this.number + ". " + this.name + "  " + this.price + "  บาท";
    }
}
