package com.chaiwat;

import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Food food1 = new Food(1, " กระเพ ราห มู กรอบ     ", 40);
        Food food2 = new Food(2, " กระเพ ราเนื้ อ         ", 40);
        Food food3 = new Food(3, " กระเพ ราห มู          ", 35);
        Food food4 = new Food(4, " กระเพ ราไก่           ", 35);
        Food food5 = new Food(5, " กระเพ ราท ะเล        ", 40);
        Food food6 = new Food(6, " กระเพ ราเค รื่องในไก่   ", 35);
        Food food7 = new Food(7, " กระเพ ราไข่ เยี่ยวม้า    ", 45);
        Food food8 = new Food(8, " คะน้าห มู กรอบ         ", 40);
        Food food9 = new Food(9, " ผั ดเผ็ ดห มูป่า         ", 40);
        Food food10 = new Food(10, "ผั ดเผ็ ดปลาดุก         ", 40);
        Food food11 = new Food(11, "ผั ดเผ็ ดกบ   ", 40);
        Food food12 = new Food(12, "ต้มยำ        ", 50);
        Food food13 = new Food(13, "สุกี้-แห้ง      ", 50);
        Food food14 = new Food(14, "สุกี้-น้ำ       ", 50);
        Food food15 = new Food(15, "ราดหน้า      ", 35);
        Food food16 = new Food(16, "ผั ดซี อิ้ว     ", 35);
        Food food17 = new Food(17, "ผั ดไทย      ", 35);
        Food food18 = new Food(18, "ข้าวไข่เจียว   ", 35);
        Food food19 = new Food(19, "ข้าวผั ดไข่    ", 35);
        Food food20 = new Food(20, "ข้าวผั ดแหนม  ", 40);
        Food food21 = new Food(21, "ยำรวมมิตร    ", 50);
        Food food22 = new Food(22, "ยำไข่เยียวม้า  ", 50);
        Food food23 = new Food(23, "ยำวุ้นเส้น     ", 50);

        ArrayList<String> foodnamelist = new ArrayList<String>();
        ArrayList<Integer> foodpricelist = new ArrayList<Integer>();
        ArrayList<Integer> foodquantitylist = new ArrayList<Integer>();

        ArrayList<Integer> totallist = new ArrayList<Integer>();

        for (int i = 1; i > 0; i++) {

            System.out.println(
                    "-----------------------------------------------------------------------------------------------------");
            System.out.println(
                    "                                        | รายการเมนู |                                         ");
            System.out.println(
                    "_____________________________________________________________________________________________________");
            System.out.print(food1);
            System.out.print("        " + food11);
            System.out.println("        " + food21);
            System.out.print(food2);
            System.out.print("        " + food12);
            System.out.println("        " + food22);
            System.out.print(food3);
            System.out.print("        " + food13);
            System.out.println("        " + food23);
            System.out.print(food4);
            System.out.println("        " + food14);
            System.out.print(food5);
            System.out.println("        " + food15);
            System.out.print(food6);
            System.out.println("        " + food16);
            System.out.print(food7);
            System.out.println("        " + food17);
            System.out.print(food8);
            System.out.print("        " + food18);
            System.out.println("        " + "101 = ยืนยันเมนูที่ สั่ง");
            System.out.print(food9);
            System.out.print("        " + food19);
            System.out.println("        " + "102 = ยกเลิกเมนู");
            System.out.print(food10);
            System.out.print("        " + food20);
            System.out.println("        " + "103 = ออกจากเมนู");
            System.out.println(
                    "_____________________________________________________________________________________________________");

            int total = 0;

            // for(int fnb :foodnumberlist){
            // System.out.print(fnb + ".");
            // for(String fn :foodnamelist){
            // System.out.print(" " + fn);
            // for(int fp :foodpricelist){
            // System.out.print(" " + fp);
            // for(int fqlt :foodquantitylist){
            // System.out.print(" " + fqlt);
            // for(int tt :totallist){
            // System.out.print(" " + tt);
            // }
            // }
            // }
            // }
            // System.out.println();
            // }

            if (foodnamelist.size() > 0) {
                System.out.println("ลำดับ" + "\t" + "ชื่อเมนู" + "\t" + "\t" + "ราคา" + "\t" + "จำนวน" + "\t" + "ยอดรวม");
                System.out.println(
                        "_____________________________________________________________________________________________________");
                for (int j = 0; j < foodnamelist.size(); j++) {
                    System.out.print((j + 1) + ".");
                    System.out.print(foodnamelist.get(j));
                    System.out.print("\t" + foodpricelist.get(j));
                    System.out.print("\t" + foodquantitylist.get(j));
                    System.out.println("\t" + totallist.get(j));
                }
            }

            System.out.println("");
            System.out.println("");

            Scanner Sc = new Scanner(System.in);
            System.out.print("ต้องการสั่งเมนูที่ :");
            int foodname = Integer.parseInt(Sc.nextLine());
        
            if(foodname == 101){

                if (foodnamelist.size() > 0) {
                    System.out.println("");
                System.out.println("**************************************************");
                System.out.println("                      Receipt                     ");
                System.out.println("**************************************************");
                System.out.println("ลำดับ" + "\t" + "ชื่อเมนู" + "\t" + "\t" + "ราคา" + "\t" + "จำนวน" + "\t" + "ยอดรวม");
                System.out.println("");
                    for (int j = 0; j < foodnamelist.size(); j++) {
                        System.out.print((j + 1) + ".");
                        System.out.print(foodnamelist.get(j));
                        System.out.print("\t" + foodpricelist.get(j));
                        System.out.print("\t" + foodquantitylist.get(j));
                        System.out.println("\t" + totallist.get(j));
                    }
                    int grandtotall = 0 ;
                    for(int e = 0 ; e < totallist.size() ; e++){
                    grandtotall = grandtotall + totallist.get(e);
                }
                    System.out.println("__________________________________________________");
                    System.out.println("");
                    System.out.println("\t"+"\t"+"\t"+"\t"+"รวมทั้งหมด       "+grandtotall);
                    System.out.println("                                           -------");
                    System.out.println("");
                    System.out.println("  ********** ข อ บ คุ ณ ที่ ใ ช้ บ ริ ก า ร **********");
                }
                else{
                    System.out.println("   ***        ไม่มีรายการเมนู         ***");
                }
    
                
                break;
            }
            if(foodname == 102){
                if(foodnamelist.size() > 0){
                Scanner Sc2 = new Scanner(System.in);
                System.out.print("ต้องการยกเลิกเมนูที่ :");
                int cancel = Integer.parseInt(Sc2.nextLine());
                cancel = cancel - 1 ; 
                if(foodnamelist.size() > cancel){
                    foodnamelist.remove(cancel);
                    foodpricelist.remove(cancel);
                    foodquantitylist.remove(cancel);
                    totallist.remove(cancel);
                }
               
                }
            }
            if(foodname == 103){
                System.out.println("");
                System.out.println("**************");
                System.out.println("ออกจากการสั่งเมนู");
                System.out.println("**************");
                break;
            }
            if (foodname == food1.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food1.getName());
                foodpricelist.add(food1.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    
                        total = foodpricelist.get(a) * foodquantitylist.get(a);
                    
                }
                totallist.add(total);
            } else if (foodname == food2.getNumber()) {
                {
                    Scanner Sc1 = new Scanner(System.in);
                    System.out.print("จำนวน :");
                    int foodquantity = Integer.parseInt(Sc1.nextLine());
                    foodnamelist.add(food2.getName());
                    foodpricelist.add(food2.getPrice());
                    foodquantitylist.add(foodquantity);
                    for (int a = 0; a < foodpricelist.size(); a++) {
                        for (int b = 0; b < foodquantitylist.size(); b++) {
                            total = foodpricelist.get(a) * foodquantitylist.get(b);
                        }
                    }
                    totallist.add(total);
                }
            } else if (foodname == food3.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food3.getName());
                foodpricelist.add(food3.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food4.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food4.getName());
                foodpricelist.add(food4.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food5.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food5.getName());
                foodpricelist.add(food5.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food6.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food6.getName());
                foodpricelist.add(food6.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food7.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food7.getName());
                foodpricelist.add(food7.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food8.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food8.getName());
                foodpricelist.add(food8.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food9.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food9.getName());
                foodpricelist.add(food9.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food10.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food10.getName());
                foodpricelist.add(food10.getPrice());
                foodquantitylist.add(foodquantity); for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food11.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food11.getName() + "\t");
                foodpricelist.add(food11.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food12.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food12.getName() + "\t"+"\t");
                foodpricelist.add(food12.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food13.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food13.getName() + "\t");
                foodpricelist.add(food13.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food14.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food14.getName() + "\t");
                foodpricelist.add(food14.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food15.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food15.getName() + "\t");
                foodpricelist.add(food15.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food16.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food16.getName() + "\t");
                foodpricelist.add(food16.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food17.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food17.getName() + "\t");
                foodpricelist.add(food17.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food18.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food18.getName() + "\t");
                foodpricelist.add(food18.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food19.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food19.getName() + "\t");
                foodpricelist.add(food19.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food20.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food20.getName() + "\t");
                foodpricelist.add(food20.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food21.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food21.getName() + "\t");
                foodpricelist.add(food21.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food22.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food22.getName() + "\t");
                foodpricelist.add(food22.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            } else if (foodname == food23.getNumber()) {
                Scanner Sc1 = new Scanner(System.in);
                System.out.print("จำนวน :");
                int foodquantity = Integer.parseInt(Sc1.nextLine());
                foodnamelist.add(food23.getName() + "\t");
                foodpricelist.add(food23.getPrice());
                foodquantitylist.add(foodquantity);
                for (int a = 0; a < foodpricelist.size(); a++) {
                    for (int b = 0; b < foodquantitylist.size(); b++) {
                        total = foodpricelist.get(a) * foodquantitylist.get(b);
                    }
                }
                totallist.add(total);
            }
        }
    }

}
